/**!
* Created By:     Chris Bennett
*
* This .js is intended to house all the code that's used for "dashboard"
* functions. I should be include in the "container" report at the which
* the portlet is pointed.
*
*/

/*jshint node: true, -W098 */
/*jslint plusplus: true, node: true, unparam: true */
/*global ICJI, $icji, log, hideGlobalPrompts
*/

ICJI.urlApi = {
    param: {
        bAction: 'b_action=',
        cvHeader: 'cv.header=',
        cvToolbar: 'cv.toolbar=',
        uiName: 'ui.name=',
        uiAction: 'ui.action=',
        uiObject: 'ui.object=',
        outputFormat: 'run.outputFormat=',
        runPrompt: 'run.prompt='
    },
    path: window.location.pathname,
    defaultFrameUri: function (rptPath, rptName) {
        var t = this.param;
        return this.path +
            '?' + t.bAction + 'cognosViewer' +
            '&' + t.uiAction + 'run' +
            '&' + t.uiObject + encodeURI(rptPath) +
            '&' + t.uiName + encodeURI(rptName) +
            '&' + t.outputFormat + // intentionally left blank
            '&' + t.runPrompt + 'false' +
            '&' + t.cvHeader + 'false' +
            '&' + t.cvToolbar + 'false';
    }
};

/**
* Tools for cleaning (hiding) Cognos Connection UI objects
* Some of these things can be done with just CSS - see icji-noRVheader.css
*/
ICJI.uiClean = {
    mainHeader: "mainHeader3",
    hideMainHead: function () {
        if ($icji('table[class="mainHeader3"]')) {
            $icji('table[class="mainHeader3"]')
                .next().css({"display": "none", "visibility": "hidden"});
            log.info("Hiding mainHeader3");
        }
    },
    /**
     * This was built for hiding the header that pops up when using a portlet
     * Seems pretty brittle to me - but project deadlines don't care
     * TODO: revisit this when time (or another project) permits
     */
    hidePortletHead: function () {
        if ($icji("#" + ICJI.getCognosViewerId() + "content").length !== 0) {
            $icji("#" + ICJI.getCognosViewerId() +
                "content").parent().parent().prev().remove();
            log.info("Hiding Portlet Icons");
        } else {
            $icji('img[src*="action_new_page.gif"]', parent.document).closest("table").parent().closest("table").hide();
        }
    },
    /**
     * Shortcut for hiding both...
     */
    portlet: function () {
        this.hideMainHead();
        this.hidePortletHead();
    }
};
/**
*  Stores functions for setting user defaults on reports/dashboard.
*/
ICJI.defaults = {
    RUNTYPE: "runType",
    RT_GET: "get",
    RT_SET: "set",
    USERNAME: "username",
    GROUPID: "groupId",
    BREADCRUMB: "breadcrumb",
    RPT_NAME: "rptName",
    RPT_PATH: "rptPath",
    DEFAULT_PARAMS: "defaultParams",
    servletURL: "",
    postSuccess: false,
    /**
     * run a post request to the servlet that gets the user defaults
     * @param data - JSON object to be passed the servlet
     */
    postJSON: function (data) {
        this.postSuccess = false;
        $icji.post(ICJI.defaults.servletURL,
            data,
            function () { ICJI.defaults.postSuccess = true; },
            "json");
    },
    /**
     * Builds the request information for retrieving the user defaults from the
     * servlet via the postJSON function
     * @param type - type of request being sent to the server
     * @param usr - username of the user running the report
     * @param grp - distinguishes between multiple reports/dashboards
     */
    get: function (type, usr, grp) {
        var data = {};
        data[this.RUNTYPE] = type;
        data[this.USERNAME] = usr;
        data[this.GROUPID] = grp;
        var resultJSON = this.postJSON(data);
        /** TODO: Build out parameter set functions base on results **/
    },
    /**
     * Builds the request information for setting the user defaults with the
     * servlet via the postJSON function
     * @param type - type of request being sent to the server
     * @param usr - username of the user running the report
     * @param grp - distinguishes between multiple reports/dashboards
     * @param brd - breadcrumb of the report location in the menu
     * @param rnm - name of the report to run by default
     * @param pth - path of the report to run by default
     * @param dpr - default settings for prompts on the report/dashboard
     */
    set: function (type, usr, grp, brd, rnm, pth, dpr) {
        var data = {};
        data[this.RUNTYPE] = type;
        data[this.USERNAME] = usr;
        data[this.GROUPID] = grp;
        data[this.BREADCRUMB] = brd;
        data[this.RPT_NAME] = rnm;
        data[this.RPT_PATH] = pth;
        data[this.DEFAULT_PARAMS] = dpr;
        this.postJSON(data);
    }
};

/**
* Sets the height of the iframe that is used to display the reports.
*/
ICJI.iframe = {
    l: 0,
    t: '',
    e: '',
    h: 350,
    id: '',
    currentReport: {
        location: "",
        name: "",
        breadcrumb: "",
        useGlobalPrompts: true,
        reportPrompts: 0
    },
    breadcrumbs: function (t) {
        $icji('#icji-frame-breadcrumbs').html('<span>' + t + '</span>');
    },
    /**
     * Function to initialize the iFrame
     * @param o = takes in a JSON object with the following objects:
     *      id - id of the iframe DOM object
     *      initialReport - container for the report that will initial be run
     *          location - cognos path of the report
     *          name - Cognos name of the report
     *          breadcrumb - path of the report in the dropdown menu
     *          useGlobalPrompts - boolean indicating if the func should get
     *                  the report global prompts
     */
    build: function (o) {
        this.id = o.id;
        if (o.initialReport !== undefined) {
            this.setSource(
                o.initialReport.location,
                o.initialReport.name,
                o.initialReport.breadcrumb,
                o.initialReport.useGlobalPrompts,
                o.initialReport.reportPrompts
            );
        }
    },
    clearLoop: function () {
        clearTimeout(this.t);
        this.loading().hide();
    },
    loading: function () {
        var _doIt = function (o) {
            $icji('#icji-frameLoader').attr('class', 'icji-frameLoad' + o);
            $icji('#icji-frameGrayed').attr('class', 'icji-frameGray' + o);
            return true;
        };
        return {
            show: function () {
                return _doIt("");
            },
            hide: function () {
                return _doIt("No");
            }
        };
    },
    print: function () {
        $icji("#" + ICJI.iframe.id)
            .contents().find("#icji-iframe-rpt-header").show();
        var frame = window.frames[ICJI.iframe.id];
        frame.focus();
        frame.print();
    },
    setHeight: function (o) {
        if (o !== undefined) {
            this.l = 0;
            this.e = o;
        }
        if (this.e.offsetTop > 0 && this.e.offsetTop === this.h) {
            $icji('#' + this.id).height((this.h + 50) + 'px');

            // for use with portal objects (use "HTML Page Viewer")
            if ($icji('iframe', parent.document).length > 0) {
                $icji('iframe', parent.document)[0].height = (this.h + 300) + 'px';
            }

            this.l = 99;
            this.clearLoop();
            ICJI.prompt.enableApplyGlobalPromptsButton();
        } else {
            this.h = this.e.offsetTop;
            if (this.l < 40) {
                this.t = setTimeout(function () {
                    ICJI.iframe.setHeight();
                }, 100);
            }
        }
    },
    setSource: function (p, n, b, g, r) {
        this.loading().show();
        var cr = this.currentReport,
            gp = "",
            loc;
        cr.location = p;
        cr.name = n;
        cr.breadcrumb = b;
        cr.useGlobalPrompts = g;
        cr.reportPrompts = r;
        loc = ICJI.urlApi.defaultFrameUri(p, n);
        this.breadcrumbs(b);
        if (cr.useGlobalPrompts) {
            gp = ICJI.prompt.getPromptString();
        }
        document.getElementById(this.id).src = loc + gp;
        log.info('Set ' + this.id + ' Source to: ' + loc + gp +
            '\n    path: ' + cr.breadcrumb + '\n    name: ' + cr.name);
        // Set global prompt options for the selected report.
        //ICJI.prompt.hideGlobalPrompts("all", true);
        $icji("div[icji-report-prompt]").hide();
        $icji("div[icji-report-prompt*='" + r + "']").show();
    }
};


/**
* Container for prompt related functions
*/
ICJI.prompt = {
    allPrompts: [],
    applyGlobalPrompts: function () {
        ICJI.iframe.setSource(
            ICJI.iframe.currentReport.location,
            ICJI.iframe.currentReport.name,
            ICJI.iframe.currentReport.breadcrumb,
            true,
            ICJI.iframe.currentReport.reportPrompts
        );
        //var o = document.getElementById("toggleGlobalPrompts");
        //this.hideGlobalPrompts(o, "GlobalPrompts");
        /** TODO: This MUST be Changed!!! */
        $icji("#RVContent" + ICJI.getCognosViewerId()).animate({ scrollTop: 0 }, "fast");
    },
    applyGlobalPromptsButton: "",
    cascade: {},
    enableApplyGlobalPromptsButton: function () {
        ICJI.getHtmlObject("div", this.applyGlobalPromptsButton)
            .find(":button")
            .prop("disabled",false);
    },
    /**
     * hides the global prompt options no clicked
     */
    hideGlobalPrompts: function (e, all) {
        var n = e.replace('toggle', ''),
            that = $icji('#' + e.replace('toggle', '')),
            allGP = $icji("div[id^='GlobalPrompts__']"),
            allGPbtn = $icji("div[id^='toggleGlobalPrompts__']"),
            o = $icji("#" + e),
            i;

        for (i = 0; i < allGP.length; i++) {
            if (allGP[i].id !== n) {
                $icji('#' + allGP[i].id).hide();
            }
            if (allGPbtn[i].id !== e) {
                $icji('#' + allGPbtn[i].id).parent().addClass("ui-state-default").removeClass("ui-state-active");
            }
        }

        if (!all) {
            if (that.is(':hidden')) {
                that.show();
                o.parent().addClass("ui-state-active").removeClass("ui-state-default");
            } else {
                that.hide();
                o.parent().addClass("ui-state-default").removeClass("ui-state-active");
            }
        }
    },
    promptsArray: [],
    /**
     * returns the url api string representation of the prompts options, that
     * have been selected in the global prompts window, so they can be applied
     * to the currently loaded report or the report that will be clicked in the
     * menu list.
     */
    getPromptString: function () {
        var p,
            i;
        if (this.allPrompts.length === 0) {
            this.setAllPrompts();
        }
        this.promptsArray = [];
        for (i = 0; i < this.allPrompts.length; i++) {
            p = this.allPrompts[i];
            if (p.type === ICJI.type.SELECTSINGLE) {
                this.test.selectSingle(p);
            } else if (p.type === ICJI.type.SELECTMULTI) {
                this.test.selectMulti(p);
            } else if (p.type === ICJI.type.CHECKBOXES) {
                this.test.checkBoxes(p);
            } else if (p.type === ICJI.type.TEXTBOX) {
                this.test.selectSingle(p);
            } else if (p.type === ICJI.type.SELECTDATE) {
                /** TODO: Define this section */
            }
        }
        return this.promptsArray.length > 0 ?
                "&" + this.promptsArray.join("&") : "";
    },
    setClick: function (isOn) {
        var allGPbtn = $icji("div[id^='toggleGlobalPrompts__']"),
            i;
        for (i = 0; i < allGPbtn.length; i++) {
            var that = $icji('#' + allGPbtn[i].id);
            if (isOn) {
                that.on('click', function (event) { ICJI.prompt.hideGlobalPrompts(event.target.id); } );
                that.parent().css('color', 'black');
                that.parent().css('cursor', 'pointer');
            } else {
                that.off('click');
                that.parent().css('color', 'gray');
                that.parent().css('cursor', 'default');
            }
        }
        return true;
    },
    test: {
        pn: function (p) { return ICJI.PARAMPREFIX + p.name + "="; },
        eC: [],
        eL: 0,
        v: "",
        pL: 0,
        pushToPA: function (o) {
            ICJI.prompt.promptsArray[ICJI.prompt.promptsArray.length] = o;
        },
        pA: this.promptsArray,
        applyEC: function () {
            if (this.eC.length > 0) {
                this.pushToPA(this.eC.join("&"));
            }
        },
        checkBoxes: function (p) {
            var j,
                o = $icji("input[name='" +
                    ICJI.getObjectInfo.oOptlNames(p.obj, "radioOptions") +
                    "']:checked");
            this.eC = [];
            for (j = 0; j < o.length; j++) {
                this.eC[this.eC.length] = this.pn(p) + o[j].value;
            }
            this.applyEC();
        },
        selectSingle: function (p) {
            this.v = document.getElementById(p.id);
            if (this.v !== undefined &&
                    this.v.value !== undefined &&
                    this.v.value !== "") {
                this.pushToPA(this.pn(p) + this.v.value);
            }
        },
        selectMulti: function (p) {
            var j;
            this.v = document.getElementById(p.id);
            this.eC = [];
            for (j = 0; j < this.v.length; j++) {
                if (this.v.options[j].selected) {
                    this.eC[this.eC.length] =
                        this.pn(p) + this.v.options[j].value;
                }
            }
            this.applyEC();
        }
    },
    /**
     * m2m = Many 2 Many (Relationship)
     * m2m provides the functionality for "cascading" prompts but handles the
     * it in multiple directions top, bottom or middle
     */
    m2m: function (o) {
        this.chartLevelName = o.chartLevelName || "";
        this.chartLevelNumInd = "!!LEVEL!!";
        this.chartLevelParam = o.chartLevelParam || "";
        this.firstStep = o.firstStep;
        this.hierData = o.hierData || {};
        this.hierJson = {};
        this.hierParams = o.hierParams || [];
        this.isMultiSelect = o.isMultiSelect || false;
        this.loc = 0;
        this.name = o.name || "(Name not Set)";
        this.opener = o.opener || "";
        this.prefixBlock = o.promptBlockPrefix || "";
        this.prefixPrmpt = o.promptPrefix || "";
        /**
         * Asyncronous build command for setting up the prompt objects on the report
         */
        this.doBuild = function () {
            log.time(this.name + " m2m.doBuild", log4javascript.Level.INFO);
            // parse the initial JSON object and setup the hierarchy
            if (this.parseData()) {
                this.hierData = {};  // clean the unused object
                this.populateSelect(this.firstStep, this.hierJson);
                this.sortAll();
            }
            this.setOnChange();
            log.timeEnd(this.name + " m2m.doBuild");
        };
        /**
         * build command for setting up the prompt objects on the report
         * async = boolean telling that calls the doBuild and lets the code finish
         */
        this.build = function (async) {
            this.setFilterIndicators(false, this.opener);
            if (async) {
                var that = this;
                log.info("Setup Async doBuild - " + this.name);
                setTimeout(function() {
                    log.info("Calling Async doBuild - " + that.name);
                    that.doBuild();
                }, 1000);
            } else {
                this.doBuild();
            }
            return this;
        };
        /**
         * Removes all options from the select object except if it's not a
         * multi-select prompt object
         * @param s - name of the prompt object to be emptied.
         */
        this.clearOptions = function (s) {
            var slct = $icji('#' +
                ICJI.getObjectInfo.oName(this.prefixPrmpt + s) +
                " option");
            if (slct.length > 0 && slct.parent()[0].multiple) {
                slct.remove();
            } else {
                slct.slice(2).remove();
            }
        };
        this.clearSelections = function () {
            //this.setFilterIndicators(false, this.opener);
            var p = this.hierParams,
                i;
            for (i = 0; i < p.length; i++) {
                this.clearOptions(p[i]);
            }
            this.populateSelect(this.hierParams[0], this.hierJson);
            this.sortAll();
            ICJI.prompt.setClick(true);
            this.setChartLevel(1);
        };
        this.parseData = function () {
            log.time(this.name + " m2m.parseData", log4javascript.Level.DEBUG);
            var i,
                d;
            for (i = 1; i <= this.hierData.count; i++) {
                d = this.hierData["d" + i];
                this.loc = 1;
                this.parseLevel(this.hierJson, d);
            }
            log.timeEnd(this.name + " m2m.parseData");
            return true;
        };
        /**
         * j = JSON object to be populated
         * d = JSON object containing the level values
         *
         * This parses the list of prompt values and creates a JSON object
         * that represents the prompt hierarchy.
         *
         * This function calls itself for each level.
         */
        this.parseLevel = function (j, d) {
            var n = this.loc,
                lvl = d["lvl" + n],
                param = this.hierParams[n] || "";
            if (!j.hasOwnProperty(lvl.code)) {
                j[lvl.code] = {};
                j[lvl.code].name = lvl.name;
                j[lvl.code].sort = lvl.sort;
                if (n !== this.hierParams.length) {
                    j[lvl.code][param] = { };
                }
            }
            if (n < this.hierParams.length) {
                this.loc = n + 1;
                this.parseLevel(
                    j[lvl.code][param],
                    d
                );
            }
        };
        /**
         * p = param name given in Cognos for the select object to populate
         * j = JSON object containing the data to load
         * k = key of the only value that should be populated
         *
         * Populates the select objects with the appropriate content.
         */
        this.populateSelect = function (p, j, k) {
            log.time(this.name + " m2m.populateSelect", log4javascript.Level.DEBUG);


            log.time(this.name + " m2m.populateSelect vars", log4javascript.Level.TRACE);
            var len = this.hierParams.length,
                nxtL = $icji.inArray(p, this.hierParams) + 1,
                nxtP = this.hierParams[nxtL],
                pSelect = $icji('#' +
                    ICJI.getObjectInfo.oName(this.prefixPrmpt + p)),
                that = this;
            log.timeEnd(this.name + " m2m.populateSelect vars");


            log.time(this.name + " m2m.populateSelect each", log4javascript.Level.TRACE);
            $icji.each(j, function (key, val) {
                if (key === k || k === undefined) {
                    if ($icji(pSelect.selector +
                            " option[value='" + key + "']").length === 0) {
                        pSelect.append(
                            $icji("<option></option>")
                                .attr("value", key)
                                .attr("sortBy", val.sort)
                                .data("prmpt-json", val)
                                .text(val.name)
                        );
                    }
                    if (nxtL < len) {
                        that.populateSelect(nxtP, val[nxtP]);
                    }
                }
            });
            log.timeEnd(this.name + " m2m.populateSelect each");

            if (!pSelect[0].multiple) {
                pSelect[0].selectedIndex = 0;
            }


            log.timeEnd(this.name + " m2m.populateSelect");
        };
        /**
         * function that runs when a select option item is selected
         */
        this.promptChange = function (e) {
            var idx = $icji(e)[0].selectedIndex,
                len = this.hierParams.length,
            // current select level data
                cPrm = $icji(e).attr("icji-param-name"),
                cLvl = $icji.inArray(cPrm, this.hierParams),
                cJsonAll = $icji("#" +
                    ICJI.getObjectInfo.oName(
                        this.prefixPrmpt + cPrm
                    ) +
                    " option:selected"),
                cJson = cJsonAll.data("prmpt-json"),
                cj,
                chartLvl = this.hierParams.length > (cLvl + 2) ? (cLvl + 2) : this.hierParams.length,
            // next select level down data
                nLvl = cLvl + 1,
                nPrm = this.hierParams[nLvl],
                i,
                pJson;

            this.setChartLevel(chartLvl);

            for (i = nLvl; i < len; i++) {
                this.clearOptions(this.hierParams[i]);
            }
            if (!this.isMultiSelect && (idx === 0 || idx === 1)) {
                if (cLvl === 0) {
                    this.populateSelect(cPrm, this.hierJson);
                } else if (cLvl !== len) {
                    // previous select level up data
                    pJson = $icji("#" + ICJI.getObjectInfo.oName(
                        this.prefixPrmpt +
                            this.hierParams[cLvl - 1]
                    ) + " option:selected").data("prmpt-json");
                    this.populateSelect(cPrm, pJson[cPrm]);
                }
            } else {
                if (cJsonAll.length > 1) {
                    for (i = 0; i < cJsonAll.length; i++) {
                        cj = $icji(cJsonAll[i]).data("prmpt-json");
                        this.populateSelect(nPrm, cj[nPrm]);
                    }
                } else {
                    this.populateSelect(nPrm, cJson[nPrm]);
                }
            }
            ICJI.prompt.setClick(false);
        };
        this.setChartLevel = function (chartLvl) {
            $icji("#" + ICJI.getObjectInfo.oName(this.chartLevelParam))
                .val(this.chartLevelName.replace(this.chartLevelNumInd, chartLvl));
        };
        this.setFilterIndicators = function (isActive, o) {
            isActive = isActive === undefined ? false : isActive;
            if (isActive) {
                o.addClass("ui-state-active").removeClass("ui-state-default");
            } else {
                o.addClass("ui-state-default").removeClass("ui-state-active");
            }
        };
        this.setOnChange = function () {
            log.time(this.name + " m2m.setOnChange", log4javascript.Level.DEBUG);
            var that = this;
            $icji.each(this.hierParams, function (i, v) {
                if (i < (that.hierParams.length - 1)) {
                    $icji("#" + ICJI.getObjectInfo.oName(
                        that.prefixPrmpt + v
                    ))
                        .attr("icji-param-name", v)
                        .change(function () {
                            that.promptChange(this);
                        });
                } else {
                    $icji("#" + ICJI.getObjectInfo.oName(
                        that.prefixPrmpt + v
                    ))
                        .change(function () {
                            that.setChartLevel(that.hierParams.length);
                        });
                }
            });
            log.timeEnd(this.name + " m2m.setOnChange");
        };
        /**
         * Sorts all the prompt objects.
         */
        this.sortAll = function () {
            log.time(this.name + " m2m.sortAll", log4javascript.Level.DEBUG);
            var p = this.hierParams;
            for (var i = 0; i < p.length; i++) {
                this.sortSelect( $icji('#' +
                        ICJI.getObjectInfo.oName(this.prefixPrmpt + p[i]))
                );
            }
            log.timeEnd(this.name + " m2m.sortAll");
        };
        /**
         * Sorts the select options prompt object.
         */
        this.sortSelect = function (s) {
            $icji(s.selector + " option")
                .sort(this.doSort)
                .appendTo(s.selector);
        };
        /**
         * Sort function - based on standard sort function but used to sort
         * on a custom sort attribute.
         */
        this.doSort = function (a, b) {
            a = a.getAttribute("sortBy");
            b = b.getAttribute("sortBy");
            return a > b ? 1 : (a < b ? -1 : 0);
        };
    },
    /**
     * Clears the onclick function of the button and then adds the
     * Apply Global Prompts function to the button.
     * @param n - name of the DIV that contains the button
     */
    overrideButtonClick: function (n) {
        var o = ICJI.getHtmlObject("div", n).find(":button");
        log.trace("overrideButtonClick: name - " + n);
        log.trace("                     length - " + o.length);
        log.trace("                     selector - " + o.selector);
        if (o.length !== 0) {
            o.attr("onclick", "")
                .click(function () {
                    $icji(this).prop("disabled",true);
                    ICJI.prompt.applyGlobalPrompts();
                });
        }
    },
    /**
     * Search for all global prompts and store their info in the allPrompts
     * parameter for use by other functions
     */
    setAllPrompts: function () {
        var params = ICJI.getAllParameterNames(),
            i;
        this.allPrompts = [];
        this.allPrompts.length = params.length;
        for (i = 0; i < params.length; i++) {
            this.allPrompts[i] = {
                obj: params[i],
                id: ICJI.getObjectInfo.oName(params[i]),
                name: ICJI.getObjectInfo.oParamName(params[i]),
                type: ICJI.getObjectInfo.oType(params[i])
            };
        }
    }
};

ICJI.menubar = {
};









/**
* ToDo:  Clean up this disaster...
*/

String.prototype.supplant = function (o) {
    return this.replace(/{([^{}]*)}/g,
        function (a, b) {
            var r = o[b];
            return typeof r === "string" || typeof r === 'number' ? r : a;
        });
};

function resetMenu(s) {
    $icji("#menuRptType").val(s);
    $icji('#mainMenu_table > tbody:first > tr').empty();
}

function parseMenuData(s) {

    log.debug('Begin parsing menu content');

    var t, l1 = 0, l2 = 1, mItm, mRpt, mSrc, mSec, mRnm, liID, p;
    var isRebuild = s === undefined ? false : true;
    var curSecCd = s === undefined ? $icji('#secCode').val() : s;
    var isEnt = s !== undefined && $icji('#secCode').val() === 'E' ? true : false;
    var e = document.getElementById('menuData'); // list of menu items

    var menuMain =
        '<td><a onmouseover=\'xpshow("m{pL1}s{pL2}",0,this);xpsmover(this);\' ' +
        'onmouseout=\'xpsmout(this);\'>{pMItm}</a><div><ul id="vbUL_m{pL1}s{pL2}"' +
        ' class="subMenu"></ul></div></td>';

    var menuMainEmpty =
        '<td><a onmouseover=\'xpshow("m{pL1}s{pL2}",0,this);xpsmover(this);\' ' +
        'onmouseout=\'xpsmout(this);\'>{pMItm}</a></td>';

    var menuMainId = 'vbUL_m{pL1}s{pL2}'

    var menuItem =
        '<li><a href="javascript:ICJI.iframe.setSource(\'{pMSrc}\', \'{pMRnm}\', \'{pMItm}\' + \' > \' + \'{pMRpt}\', true, {mMRgp});">' +
        '{pMRpt}</a></li>';

    var menuItemGray = '<li><a style="color: #BBBBBB;">{pMRpt}</a></li>';

    log.info('Length: ' + e.childNodes.length);

    for (var i = 0; i < e.childNodes.length; i++) {

        t = e.childNodes[i].innerHTML;

        if (t !== undefined && t.indexOf('HRD') !== -1) {

            mRpt = t.split('::')[2].replace(/'/g, "\\\'");
            mSrc = t.split('::')[3].replace(/'/g, "\\\'");
            mSec = t.split('::')[4];
            mRnm = t.split('::')[5].replace(/'/g, "\\\'");
            mRgp = t.split('::')[6].replace(/'/g, "\\\'");

            log.info('mRpt:'+mRpt+'  mSrc:'+mSrc+'  mSec:'+mSec+'  mRnm:'+mRnm+'  mRgp:'+mRgp);

            if (mItm !== t.split('::')[1]) {

                mItm = t.split('::')[1];

                ++l1;

                p = {pL1: l1, pL2: l2, pMItm: mItm, pMRpt: mRpt, pMSrc: mSrc, pMSec: mSec, pMRnm: mRnm, mMRgp: mRgp};

                if (t.split('::')[2] !== ' '
                    && t.split('::')[2] !== ''
                    && t.split('::')[2] !== undefined) {

                    $icji('#mainMenu_table > tbody:first > tr')
                        .append(menuMain.supplant(p));

                    if (curSecCd === 'E' || mSec === 'E' || curSecCd === mSec) {
                        $icji('#' + menuMainId.supplant(p)).empty();
                        $icji('#' + menuMainId.supplant(p)).append(menuItem.supplant(p));
                    } else if (isEnt) {
                        $icji('#' + menuMainId.supplant(p)).append(menuItemGray.supplant(p));

                    }

                } else {
                    // insert and empty menu item
                    $icji('#mainMenu_table > tbody:first > tr')
                        .append(menuMainEmpty.supplant(p));
                }

            } else if (mItm === t.split('::')[1]) {

                if (t.split('::')[2] !== '' || t.split('::')[2] !== undefined) {

                    p = {pL1: l1, pL2: l2, pMItm: mItm, pMRpt: mRpt, pMSrc: mSrc, pMSec: mSec, pMRnm: mRnm, mMRgp: mRgp};

                   if (curSecCd === 'E' || mSec === 'E' || curSecCd === mSec) {
                        $icji('#' + menuMainId.supplant(p)).append(menuItem.supplant(p));
                    } else if (isEnt) {
                        $icji('#' + menuMainId.supplant(p)).append(menuItemGray.supplant(p));
                    }
                }
            }
        }
    }

    //rebuild the dropdowns
    if (isRebuild) {
        $icji('#xpMenuCont').empty()
        log.info('Completed Rebuild');
        vistaButtons({ subFrame: 0 }, true);
    }

    log.debug('End parsing menu content');

}

